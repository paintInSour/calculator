package model;

import java.util.LinkedList;
import java.util.List;


public class Model {
    private List<Float> numbers = new LinkedList<>();
    private String operator;
    
    public List<Float> getNumbers() {
        return numbers;
    }
    public void setNumbers(List<Float> numbers) {
        this.numbers = numbers;
    }
    public String getOperator() {
        return operator;
    }
    public void setOperator(String operator) {
        this.operator = operator;
    }
    
}
