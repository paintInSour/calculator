package input;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import calculator.CalculatorService;

public class InputServiceImpl implements InputService {

    private CalculatorService calculatorService = new CalculatorService();

    @Override
    public void startProgram() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String inputString;
        try {
            while (!(inputString = reader.readLine()).equalsIgnoreCase("q")) {
                calculatorService.read(inputString);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
