package calculator;

import model.Model;
import output.OutputServiceImpl;

public class CalculatorService {
    private OutputServiceImpl output = new OutputServiceImpl();
    private Model input = new Model();
    private Calculator calculator = new Calculator();

    public void read(String inputString) {
        if (isNumeric(inputString)) {
            input.getNumbers().add(Float.parseFloat(inputString));
        } else if (inputString.matches("[\\+\\-\\*\\/]")) {
            if (input.getNumbers().size() >= 2) {
                input.setOperator(inputString);
                calculator.calculate(input);
                output.print(input);
            } else {
                System.out.println("Operator ignored. Operands count <2");
            }
        } else {
            input.getNumbers().clear();
            System.out.println("Invalid input. All input cleared");
        }
    }

    private boolean isNumeric(String input) {
        try {
            Float.parseFloat(input);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

}
