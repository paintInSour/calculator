package calculator;

import model.Model;

public class Calculator {

    public void calculate(Model input) {

        Float op1 = input.getNumbers().get(input.getNumbers().size() - 2);
        input.getNumbers().remove(input.getNumbers().size() - 2);
        Float op2 = input.getNumbers().get(input.getNumbers().size() - 1);
        input.getNumbers().remove(input.getNumbers().size() - 1);

        input.getNumbers().add(doCalc(input.getOperator().charAt(0), op1, op2));

    }

    private Float doCalc(char operator, Float op1, Float op2) {

        switch (operator) {
            case '+':
                return op1 + op2;
            case '-':
                return op1 - op2;
            case '/':
                return op1 / op2;
            default:
                return op1 * op2;
        }
    }
}
