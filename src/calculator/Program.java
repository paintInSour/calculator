package calculator;

import java.io.IOException;

import input.InputServiceImpl;

public class Program {
       public static void main(String[] args) throws IOException {
          InputServiceImpl inputService = new InputServiceImpl();
          inputService.startProgram();
       }
}
