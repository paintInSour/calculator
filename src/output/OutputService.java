package output;

import model.Model;

public interface OutputService {
    public void print(Model input);
}
