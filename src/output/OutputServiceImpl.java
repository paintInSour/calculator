package output;

import model.Model;

public class OutputServiceImpl implements OutputService {

    @Override
    public void print(Model input) {
        System.out.println(input.getNumbers().get(input.getNumbers().size()-1));
    }

}
